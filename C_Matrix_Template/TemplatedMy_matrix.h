/*
 * @Author: Jason A. Kolodziej 
 * @Copyright (c) 2017-2019 Kolodziej Technologies. All rights reserved. 
 * @Date: 2018-01-18 09:51:43 
 * @Last Modified by: Jason A. Kolodziej
 * @Last Modified time: 2019-02-16 09:52:18
 */
// Templated file for TemplatedMy_matrix *.h & *.cpp

#ifndef CSCE_221_TEMPLATEDMY_MATRIX_H
#define CSCE_221_TEMPLATEDMY_MATRIX_H
#include <iostream> // apart of .H
#include <exception> // apart of .H
#include <stdexcept> // apart of .CPP
// .H PORTION
using namespace std;
// Definitions of user defined type exceptions
struct invalid_input : public exception {
    virtual const char* what() const throw()
    { return "Invalid matrix input"; }
};

struct incompatible_matrices : public exception {
    virtual const char* what() const throw()
    { return "Incompatible matrices"; }
};

struct empty_matrix : public exception {
    virtual const char* what() const throw()
    { return "empty matrix"; }
};
template<typename T> class TemplatedMy_matrix;// TEMPLATER for  TemplatedMy_matrix class
template<typename T>
class TemplatedMy_matrix {

    //member variables
    int n=0; int m=0; //initialized at 0 for empty
    T **ptr;
    void allocate_memory();  // optional

public:
    //member functions
    TemplatedMy_matrix();  // default constructor
    TemplatedMy_matrix(int n1, int m1); // parameter constructor
    ~TemplatedMy_matrix(); // destructor
    TemplatedMy_matrix(const TemplatedMy_matrix<T>& mat); // copy constructor
    TemplatedMy_matrix(TemplatedMy_matrix<T>&& mat);  // move constructor (optional)
    TemplatedMy_matrix<T>& operator=(const TemplatedMy_matrix<T>& mat); //copy assignment operator
    TemplatedMy_matrix<T>& operator=(TemplatedMy_matrix<T>&& mat); // move assignment operator (optional)
    int number_of_rows() const;
    int number_of_columns() const;
    T* operator()(int i) const; // overloaded to access to ith row
    T& operator()(int i, int j); // overloaded to access (i,j) element
    T operator()(int i, int j) const; // overloaded to access (i,j) element
    T elem(int i, int j) const; // overloaded to access (i,j) element
    T& elem(int i, int j); // overloaded to access (i,j) element
};
template<typename T>
// see the handout for the description of these operators
ostream& operator<<(ostream& out, const TemplatedMy_matrix<T>& mat);
template < typename T>
istream& operator>>(istream& in, TemplatedMy_matrix<T>& mat);
template < typename T>
TemplatedMy_matrix<T> operator+(const TemplatedMy_matrix<T>& mat1, const TemplatedMy_matrix<T>& mat2);
template < typename T>
TemplatedMy_matrix<T> operator*(const TemplatedMy_matrix<T>& mat1, const TemplatedMy_matrix<T>& mat2);
template<typename T>
// .CPP PORTITON
TemplatedMy_matrix<T>::TemplatedMy_matrix()
{
    // add your code here, default w/ no PARAMETERS
}
template < typename T>
void TemplatedMy_matrix<T>::allocate_memory()
{
    try{
        if(n==0 || m==0) throw new empty_matrix;
        else
        {
            this->ptr = new T*[n]; // allocate an array of row pointers
            for (auto i = 0; i < n; i++) ptr[i] = new T[m]; // allocate the i-th row
        }
    }
    catch(exception& e) {
        cerr <<  e.what() << endl;
    }

}
template < typename T>
TemplatedMy_matrix<T>::TemplatedMy_matrix(int n1, int m1)
        :n{n1},m{m1}
{
    this->ptr = new T*[n1]; // allocate an array of row pointers
    for (auto i = 0; i < n1; i++) ptr[i] = new T[m1]; // allocate the i-th row
}
template < typename T>
TemplatedMy_matrix<T>::TemplatedMy_matrix(const TemplatedMy_matrix<T>& mat)
{ //take mat's lvalue
    this->n = mat.number_of_rows();
    this->m = mat.number_of_columns();
    this->ptr = new T*[mat.number_of_rows()];
    for(auto i = 0; i < n; i++) ptr[i] = new T[mat.number_of_columns()];
    for (auto i = 0; i < mat.number_of_rows(); i++)
        for (auto j = 0; j < mat.number_of_columns(); j++)
            this->ptr[i][j] = mat.ptr[i][j];
}
template < typename T>
// move constructor (optional)
TemplatedMy_matrix<T>::TemplatedMy_matrix(TemplatedMy_matrix<T>&& mat) { //take mat's rvalue
    this->n = mat.number_of_rows();
    this->m = mat.number_of_columns();
    this->ptr = mat.ptr;
    mat.m = 0;
    mat.n = 0;
    mat.ptr = nullptr;
}
template < typename T>
TemplatedMy_matrix<T>::~TemplatedMy_matrix() //MIGHT casue issue
{
    for (auto i = 0; i < this->n; i++)
        delete[] ptr[i]; // delete the i-th row
    delete[] ptr; // delete the array of row pointers
    n=0;
    m=0;
}
template < typename T>
TemplatedMy_matrix<T>& TemplatedMy_matrix<T>::operator=(const TemplatedMy_matrix<T>& mat) { //take mat's lvalue

   if(this !=&mat) {
        delete[] ptr;
    this->n = mat.number_of_rows();
    this->m = mat.number_of_columns();
    ptr = new T *[mat.number_of_rows()];
    for (auto i = 0; i < n; i++) ptr[i] = new T[mat.number_of_columns()];
    for (auto i = 0; i < mat.number_of_rows(); i++)
        for (auto j = 0; j < mat.number_of_columns(); j++)
            this->ptr[i][j] = mat.elem(i, j);
    }
    return *this;
}

// move assignment operator (optional) take rvalue
template < typename T>
TemplatedMy_matrix<T>& TemplatedMy_matrix<T>::operator=(TemplatedMy_matrix<T>&& mat)
{
    if(this!=&mat)
    {
        this->n = mat.number_of_rows();
        this->m = mat.number_of_columns();
        this->ptr = mat.ptr; //take mat's mem of the pointer
        mat.m = 0;
        mat.n = 0;
        mat.ptr = nullptr;
    }
    return *this;
}
template < typename T>
int TemplatedMy_matrix<T>::number_of_rows() const
{
    return n;
}
template < typename T>
int TemplatedMy_matrix<T>::number_of_columns() const
{
    return m;
}
template < typename T>
T* TemplatedMy_matrix<T>::operator()(int i) const
{
    if (i < 0 || i >= n) throw out_of_range("Out of range");
    else return ptr[i];
}
template < typename T>

T TemplatedMy_matrix<T>::operator()(int i, int j) const
{
    if (i < 0 || i >= n) throw out_of_range("Out of range");
    if (j < 0 || j >= m) throw out_of_range("Out of range");
    else return ptr[i][j];
}
template < typename T>

T& TemplatedMy_matrix<T>::operator()(int i, int j)
{
    if (i < 0 || i >= n) throw out_of_range("Out of range");
    if (j < 0 || j >= m) throw out_of_range("Out of range");
    else return ptr[i][j];
}
template < typename T>

T TemplatedMy_matrix<T>::elem(int i, int j) const
{
    if (i < 0 || i >= n) throw out_of_range("Out of range");
    if (j < 0 || j >= m) throw out_of_range("Out of range");
    else return ptr[i][j];
}
template < typename T>

T& TemplatedMy_matrix<T>::elem(int i, int j)
{
    if (i < 0 || i >= n) throw out_of_range("Out of range");
    if (j < 0 || j >= m) throw out_of_range("Out of range");
    else return ptr[i][j];
}

template < typename T>
ostream& operator<<(ostream& out, const TemplatedMy_matrix<T>& mat) // output to command line
{
    for (auto i = 0; i < mat.number_of_rows(); i++) {
        for (auto j = 0; j < mat.number_of_columns(); j++) out << mat(i, j) << " "; //This line is the culprit ****
        out << endl;
    }
    return out;
}

//last ones
template < typename T>
istream& operator>>(istream& in, TemplatedMy_matrix<T>& mat) //insert from command line
{
    try {
        if(mat.number_of_columns()==0 || mat.number_of_rows() ==0) throw new empty_matrix;
        for (auto i = 0; i < mat.number_of_rows(); i++)
            for (auto j = 0; j < mat.number_of_columns(); j++)
                in >> mat.elem(i,j);
        
        if(in.peek() == '\0') throw new invalid_input;
        else if(in.peek() == ' ') throw new invalid_input;
        return in;
    }
    catch(exception& e)
    {
        cerr << e.what() << "\n";
    }
}
template < typename T>
TemplatedMy_matrix<T> operator+(const TemplatedMy_matrix<T>& mat1, const TemplatedMy_matrix<T>& mat2)
{
    try{
        if(mat1.number_of_columns()!=mat2.number_of_columns() || mat1.number_of_rows()!=mat2.number_of_rows()) throw new incompatible_matrices;
        else{
            TemplatedMy_matrix<T> ans(mat1.number_of_rows(),mat1.number_of_columns());
            for(auto i=0;i<mat1.number_of_rows(); ++i)
                for(auto j=0;j<mat1.number_of_columns(); ++j) ans.elem(i,j) = mat1.elem(i,j) + mat2.elem(i,j);
            return ans;
        }
    }
    catch(exception& e)
    {
        cerr << e.what() << "\n";
    }
}
template < typename T>
TemplatedMy_matrix<T> operator*(const TemplatedMy_matrix<T>& mat1, const TemplatedMy_matrix<T>& mat2)
{
    try{
        if(mat1.number_of_rows()!=mat2.number_of_columns() || mat2.number_of_rows()!=mat1.number_of_columns()) throw new incompatible_matrices;
        else{
            TemplatedMy_matrix<T> ans(mat1.number_of_rows(),mat2.number_of_columns());
            for(auto i=0;i<mat1.number_of_rows(); ++i)
                for(auto j=0;j<mat2.number_of_columns(); ++j)
                {
                    ans.elem(i,j) = 0;
                    for (auto k = 0; k < mat2.number_of_rows(); k++)
                        ans.elem(i,j) = ans.elem(i,j) + mat1.elem(i,k) * mat2.elem(k,j);
                }
            return ans;
        }
    }
    catch(exception& e)
    {
        cerr << e.what() << "\n";
    }
}


#endif //CSCE_221_TEMPLATEDMY_MATRIX_H
