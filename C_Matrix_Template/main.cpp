/*
 * @Author: Jason A. Kolodziej 
 * @Copyright (c) 2017-2019 Kolodziej Technologies. All rights reserved. 
 * @Date: 2018-01-18 09:51:43 
 * @Last Modified by: Jason A. Kolodziej
 * @Last Modified time: 2019-02-16 09:52:18
 */
// main.cpp
// Tests all functionality of the class TemplatedMy_matrix <int> class.

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>
#include "TemplatedMy_matrix.h"

int main(){
  
  // Some test cases are expected to throw an exception.
  // Add more try and catch blocks as necessary for your code
  // to finish execution.
    
  try{
    
    // Test 1
    // * Create an object of type TemplatedMy_matrix <int>, called m1, using constructor
    // * Initialize m1 in main (by assigning numbers to matrix elements)
    // * Display m1 on the screen using the operator <<

      cout << "test 1: with char\n";
      TemplatedMy_matrix<char> m1(2,2);
      cout << "Enter a 2x2 matrix:\n";
      cin >> m1;
      cout << m1;

    // Test 2
    // * Create an object of type TemplatedMy_matrix <int>, called m2, using (default)
    //   constructor
    // * Open an input file containing a matrix (row by row)
    // * Initialize m2 by reading from the input file using
    //   the operator >>
    // * Open an output file (can be empty)
    // * Write m2 to the output file using the operator <<

      cout << "test 2: with type double\n";
      int lines = 0; //row
      int spaces = 0; // column
      cout << "opening file & inserting file 'file.txt'\n";
      std::ifstream infile("file.txt");
      string str((std::istreambuf_iterator<char>(infile)),
                 std::istreambuf_iterator<char>());
      infile.close();
      for(auto pos : str)
      {
          if(pos == ' ') ++spaces;
          if(pos == '\n') ++lines;
      }
      for(int pos = 0; pos!=str.size(); ++pos)
          if(str[pos] == '\n') str[pos] = ' ';
      TemplatedMy_matrix <double> m2(spaces,lines);
      std::ofstream outfile("o_file.txt");
      stringstream ss;
      ss << str;
      ss >> m2;
      outfile << m2;
      outfile.close();
      
      
      // Test 2 w/ strings
      cout << "test 2: with type string\n";
      lines = 0; //row
      spaces = 0; // column
      cout << "opening file & inserting file 'fileWstrings.txt'\n";
      std::ifstream inSfile("fileWstrings.txt");
      string strS((std::istreambuf_iterator<char>(inSfile)),
                 std::istreambuf_iterator<char>());
      inSfile.close();
      for(auto pos : strS)
      {
          if(pos == ' ') ++spaces;
          if(pos == '\n') ++lines;
      }
      for(int pos = 0; pos!=strS.size(); ++pos)
          if(strS[pos] == '\n') strS[pos] = ' ';
      TemplatedMy_matrix <string> m7(spaces,lines);
      std::ofstream outfileS("o_file_string.txt");
      stringstream ssS;
      ssS << strS;
      ssS >> m7;
      outfileS << m7;
      outfileS.close();
      

    // Test 3
    // * Use the copy constructor to make a copy of m1 called m3
    // * Apply the copy assignment to m1 called m4
    // * Display m3 and m4 on the screen using the operator <<

      cout << "test 3: from test 1 and char type\n";
      TemplatedMy_matrix <char> m3(m1);
      cout << m3 << "here comes m4 \n";
      TemplatedMy_matrix <char> m4 = m1;
      cout << m4;

    // Test 4
    // * Test the matrix multiplication operator (operator*)
    // * Apply the multiplication operator to valid and invalid cases
    // * Display the resulting matrix and its number of rows and columns
//      cout << "test 4:\n";
//      cout << m1*m1 << "valid\n";
      //cout << m1*m2; // invalid

    // Test 5
    // * Test the matrix addition operator (operator+)
    // * Apply the multiplication operator to valid and invalid cases
    // * Display the resulting matrix and its number of rows and columns

//      cout << "test 5:\n";
//      cout << m1 +m4 << "valid\n";
     // cout << m1+m2; //invalid
  } catch(exception &error){
    cerr << "Error: " << error.what() << endl;
  }
}
