/*
 * @Author: Jason A. Kolodziej 
 * @Copyright (c) 2017-2019 Kolodziej Technologies. All rights reserved. 
 * @Date: 2018-04-08 09:51:43 
 * @Last Modified by: Jason A. Kolodziej
 * @Last Modified time: 2019-02-16 09:55:31
 */
#include "MinQueueTemplate.h"
#include <iostream>
#include <string>
#include <cstdio>
#include <sstream>

using namespace std;

int main () {
    try{
  // Construct a linked list with header & trailer
  cout << "Create a new list" << endl;
  DoublyLinkedList<string> dll;
  cout << "list: " << dll << endl << endl;

  // Insert 10 nodes at back with value 10,20,30,..,100
  cout << "Insert 10 nodes at back with value 10,20,30,..,100" << endl;
  for (int i=10;i<=100;i+=10) {
    stringstream ss;
    ss << i;
    dll.insertLast(ss.str());
  }
  cout << "list: " << dll << endl << endl;

  // Insert 10 nodes at front with value 10,20,30,..,100
  cout << "Insert 10 nodes at front with value 10,20,30,..,100" << endl;
  for (int i=10;i<=100;i+=10) {
    stringstream ss;
    ss << i;
    dll.insertFirst(ss.str());
  }
  cout << "list: " << dll << endl << endl;
  
  // Copy to a new list
  cout << "Copy to a new list" << endl;
  DoublyLinkedList<string> dll2(dll);
  cout << "list2: " << dll2 << endl << endl;
  
  // Assign to another new list
  cout << "Assign to another new list" << endl;
  DoublyLinkedList<string> dll3;
  dll3=dll;
  cout << "list3: " << dll3 << endl << endl;
  
  // Delete the last 10 nodes
  cout << "Delete the last 10 nodes" << endl;
  for (int i=0;i<10;i++) {
    dll.removeLast();
  }
  cout << "list: " << dll << endl << endl;
  
  // Delete the first 10 nodes
  cout << "Delete the first 10 nodes" << endl;
  for (int i=0;i<10;i++) {
    dll.removeFirst();
  }
  cout << "list: " << dll << endl << endl;
  
  // Check the other two lists
  cout << "Make sure the other two lists are not affected." << endl;
  cout << "list2: " << dll2 << endl;
  cout << "list3: " << dll3 << endl << endl;

  // more testing...
  // add tests for insertAfter, insertBefore
  cout << "Inserting 66 after first value in dll2 " << endl;
  dll2.insertAfter(*dll2.getFirst(),"66");
  cout << "list: " << dll2 << endl << endl;

  cout << "Inserting 66 before dll3 first value " << endl;
  dll3.insertBefore(*dll3.getFirst(),"66");
  cout << "list: " << dll3 << endl << endl;

  // add tests for removeAfter, removeBefore
  cout << "Deleting 66 after first value in dll2 " << endl;
  dll2.removeAfter(*dll2.getFirst());
  cout << "list: " << dll2 << endl << endl;

  cout << "Deleting 66 before dll3 second value " << endl;
  dll3.removeBefore(*dll3.getFirst()->next);
  cout << "list: " << dll3 << endl << endl;

  // add tests for DoublyLinkedListLength
  cout << "Check length values of dll2 and dll3 " << endl;
  cout << "dll2 length: " << DoublyLinkedListLength(dll2) << endl << endl;
  cout << "dll3 length: " << DoublyLinkedListLength(dll3) << endl << endl;
    cout << "Testing MinQueue class & inserting 4,7,1,9" << endl;
    MinQueue<string> t;
    cout << "Is MinQueue empty? (Before Enqueue) " << (t.isEmpty() ? "Yes" : "No") << endl;
    t.enqueue("4");
    t.enqueue("7");
    t.enqueue("1");
    t.enqueue("9");
    cout << "After inserting said elements the size of the Queue is "<< t.size() << endl;
    cout << "The minimum value in the Queue is " << t.min() << endl;
    cout << "Dequeue 4,7,1,9" << endl;
    cout << t.dequeue() << endl;
    cout << t.dequeue() << endl;
    cout << t.dequeue() << endl;
    cout << t.dequeue() << endl;
    cout << "Is MinQueue empty? (After Dequeue) " << (t.isEmpty() ? "Yes" : "No") << endl;
    }
    catch(...)
    {
        cerr << "some exception was caught\n" <<endl;
        
    }
  
  return 0;
}
