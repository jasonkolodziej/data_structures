/*
 * @Author: Jason A. Kolodziej 
 * @Copyright (c) 2017-2019 Kolodziej Technologies. All rights reserved. 
 * @Date: 2018-04-08 09:51:43 
 * @Last Modified by: Jason A. Kolodziej
 * @Last Modified time: 2018-04-08 09:52:18
 */
#include "TemplateDoublyLinkedList.h"
#ifndef PAS_MINQUEUETEMPLATE_H
#define PAS_MINQUEUETEMPLATE_H

template<typename T> class MinQueue {
private:
    T x;
    DoublyLinkedList<T> M;
public:
    MinQueue(){};
    ~MinQueue();
    void enqueue(T); // inserts the int to the end of the list
    T dequeue(); // returns the value at the head of the list and and deletes from the list
    int size(){return DoublyLinkedListLength(M);}; // returns the size of the list
    T min();
    bool isEmpty(){ return M.isEmpty();};
};

template<typename T>
MinQueue<T>::~MinQueue(){
    x.~T();
    M.~DoublyLinkedList();
}

template<typename T>
void MinQueue<T>::enqueue(T x) {
    M.insertLast(x);
}

template<typename T>
T MinQueue<T>::dequeue() {
    if (!isEmpty()){
        T r = M.first();
        M.removeFirst();
        return r;
    }
}

template<typename T>
T MinQueue<T>::min() {
    DListNode<T> *current = M.getFirst(); // returns the node header.next points to
    T ans = current->obj;
    while(current != M.getAfterLast()) {
        //find min
        if (current->obj < ans) ans = current->obj;
        //iterate through list
        current = current->next;
    }
    return ans;
}

#endif //PAS_MINQUEUETEMPLATE_H
