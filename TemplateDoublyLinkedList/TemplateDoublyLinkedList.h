/*
 * @Author: Jason A. Kolodziej 
 * @Copyright (c) 2017-2019 Kolodziej Technologies. All rights reserved. 
 * @Date: 2018-04-08 09:51:43 
 * @Last Modified by: Jason A. Kolodziej
 * @Last Modified time: 2018-04-08 09:52:18
 */
#include <cstdlib>
#include <iostream>
#include <stdexcept>
using namespace std;
 //Template definition
// list node
template <typename T> struct DListNode {
    T obj;
    DListNode *prev, *next;
    DListNode(DListNode *p = NULL, DListNode *n = NULL)
            : prev(p), next(n) {}
};

// doubly linked list
template<typename T> class DoublyLinkedList {
private:
    DListNode<T> header, trailer;
public:
    DoublyLinkedList()// constructor
    { header.next = &trailer; trailer.prev = &header; }
    DoublyLinkedList(const DoublyLinkedList<T>& dll); // copy constructor
    ~DoublyLinkedList(); // destructor
    DoublyLinkedList<T>& operator=(const DoublyLinkedList<T>& dll); // assignment operator
    // return the pointer to the first node
    DListNode<T> *getFirst() const { return header.next; }
    // return the pointer to the trailer
    const DListNode<T> *getAfterLast() const { return &trailer; }
    // return if the list is empty
    bool isEmpty() const { return header.next == &trailer; }
    T first() const; // return the first object
    T last() const; // return the last object
    void insertFirst(T newobj); // insert to the first of the list
    void removeFirst(); // remove the first node
    void insertLast(T newobj); // insert to the last of the list
    void removeLast(); // remove the last node
    void insertAfter( DListNode<T> &p, T newobj);
    void insertBefore( DListNode<T> &p, T newobj);
    void removeAfter( DListNode<T> &p);
    void removeBefore( DListNode<T> &p);
};
template<typename T>
// output operator
ostream& operator<<(ostream& out, const DoublyLinkedList<T>& dll);
// return the list length
template<typename T>
int DoublyLinkedListLength(DoublyLinkedList<T>& dll);

// extend range_error from <stdexcept>
struct EmptyDLinkedListException : std::range_error {
    explicit EmptyDLinkedListException(char const* msg=NULL): range_error(msg) {}
};

// copy constructor
template<typename T>
DoublyLinkedList<T>::DoublyLinkedList(const DoublyLinkedList<T>& dll)
{
    // Initialize the list
    header.next = &trailer; trailer.prev = &header;
    DListNode<T> *current = dll.getFirst(); // returns the node header.next points to
    while(current != dll.getAfterLast()) {
        this->insertLast(current->obj);
        //iterate through list
        current = current->next;
    }

}

// assignment operator
template<typename T>
DoublyLinkedList<T>& DoublyLinkedList<T>::operator=(const DoublyLinkedList<T>& dll) {
    // Delete the whole list
    if(!isEmpty()) delete this; ///MIGHT CAUSE AN ISSUE
    if(this!=&dll){
        DListNode<T> *c_node = dll.getFirst();
        while(c_node!=&dll.trailer){
            this->insertLast(c_node->obj);
            c_node = c_node->next;
        }
    }
    return *this;
}

// insert the new object as the first one
template<typename T>
void DoublyLinkedList<T>::insertFirst(T newobj)
{
    DListNode<T> *n = new DListNode<T>(&header, header.next);
    n->obj = newobj;
    header.next->prev = n;
    header.next = n;
}

// insert the new object as the last one
template<typename T>
void DoublyLinkedList<T>::insertLast(T newobj)
{
    DListNode<T> *n = new DListNode<T>(trailer.prev, &trailer);
    n->obj = newobj;
    trailer.prev->next = n;
    trailer.prev = n;
}

// remove the first object from the list
template<typename T>
void DoublyLinkedList<T>::removeFirst()
{
    try{
        if (isEmpty()) throw EmptyDLinkedListException("EMPTY doubly linked list");
        DListNode<T> *tmp = header.next;
        tmp->next->prev = &header;
        // have the header's next pointer store the mem addr tmp's next pointer is pointing to
        header.next = tmp->next;
        delete tmp;
    }
    catch (EmptyDLinkedListException& r) {
        cerr << r.what() << endl;
    }
}

// remove the last object from the list
template<typename T>
void DoublyLinkedList<T>::removeLast()
{
    try {
        if (isEmpty()) throw EmptyDLinkedListException("EMPTY doubly linked list");
        DListNode<T> *tmp = trailer.prev;
        // set the node's, pointed to by the value stored in tmp prev,
        //next value to be the mem addr of trailer
        tmp->prev->next = &trailer;
        // have the trialer's prev pointer store the mem addr tmp's prev pointer is pointing to
        trailer.prev = tmp->prev;
        delete tmp;
    }
    catch (EmptyDLinkedListException& r) {
        cerr << r.what() << endl;
    }

}

// destructor
template<typename T>
DoublyLinkedList<T>::~DoublyLinkedList()
{
    // node pointers made for previous and current points in list
    DListNode<T> *prev, *c_node = header.next;
    while(c_node!=&trailer){
        prev = c_node;
        c_node = c_node->next;
        delete prev;
    }
    header.next = &trailer;
    trailer.prev = &header;
}

// return the first object
template<typename T>
T DoublyLinkedList<T>::first() const
{
    if (isEmpty()) throw EmptyDLinkedListException("EMPTY doubly linked list");
    return header.next->obj;
}


// return the last object
template<typename T>
T DoublyLinkedList<T>::last() const
{
    if (isEmpty()) throw EmptyDLinkedListException("EMPTY doubly linked list");
    return trailer.prev->obj;
}

// insert the new object after the node p
template<typename T>
void DoublyLinkedList<T>::insertAfter( DListNode<T> &p, T newobj) {
    try {
        if (&p == &trailer) throw EmptyDLinkedListException("Bad allocation access to memory location");
        DListNode<T> *current = getFirst(); // returns the node header.next points to
        while (current != this->getAfterLast()) {
            if (current->obj == p.obj) {
                DListNode<T> *insert = new DListNode<T>; // create new node
                insert->obj = newobj;
                insert->prev = current;
                insert->next = current->next;
                current->next = insert;
                insert->prev->next = insert;
                break;
            }
            //iterate through list
            current = current->next;
        }
    }
    catch (EmptyDLinkedListException& r) {
        cerr << r.what() << endl;
    }
}

// insert the new object before the node p
template<typename T>
void DoublyLinkedList<T>::insertBefore( DListNode<T> &p, T newobj) {
    try {
        if (&p == &trailer) throw new EmptyDLinkedListException("Bad allocation access to memory location");
        DListNode<T> *current = this->getFirst(); // returns the node header.next points to
        while (current != this->getAfterLast()) {
            if (current->obj == p.obj) {
                DListNode<T>* insert = new DListNode<T>; // create new node
                insert->obj = newobj;
                insert->next = current;
                insert->prev = current->prev;
                current->prev = insert;
                insert->prev->next = insert;
                break;
            }
            //iterate through list
            current = current->next;
        }
    }
    catch (EmptyDLinkedListException& r) {
        cerr << r.what() << endl;
    }

}
// remove the node after the node p
template<typename T>
void DoublyLinkedList<T>::removeAfter( DListNode<T> &p){
    try {
        if (&p == &trailer || &p == &header) throw EmptyDLinkedListException("Bad allocation access to memory location");
        DListNode<T> *current = this->getFirst(); // returns the node header.next points to
        while (current != this->getAfterLast()) {
            if (current->obj == p.obj) {
                DListNode<T> *d = current->next; //set to the node you are removing (after node 'p')
                d->next->prev = current; // set the next node that d points to prev value to equal current
                current->next = d->next; //set the prev node that current points to what 'd' next points to
                delete d;
                break;
            }
            //iterate through list
            current = current->next;
        }
    }
    catch(EmptyDLinkedListException& r) {
        cerr << r.what() << endl;
    }

}



// remove the node before the node p
template<typename T>
void DoublyLinkedList<T>::removeBefore( DListNode<T> &p) {
    try {
        if (&p == &header || &p==&trailer) throw EmptyDLinkedListException("Bad allocation access to memory location");
        DListNode<T> *current = this->getFirst(); // returns the node header.next points to
        while (current != this->getAfterLast()) {
            if (current == &p) {
                DListNode<T> *d = current->prev; //set to the node you are removing (before node 'p')
                d->prev->next = current; // set the previous node that d points to next value to equal current
                current->prev = d->prev; //set the prev node that current points to what 'd' previous points to
                delete d;
                break;
            }
            //iterate through list
            current = current->next;
        }
    }
    catch(EmptyDLinkedListException& r) {
        cerr << r.what() << endl;
    }

}

// return the list length
template<typename T>
int DoublyLinkedListLength(DoublyLinkedList<T>& dll)
{
    DListNode<T> *current = dll.getFirst(); // returns the node header.next points to
    int count = 0;
    while(current != dll.getAfterLast()) {
        count++;
        //iterate through list
        current = current->next;
    }
    return count;

}

// output operator
template<typename T>
ostream& operator<<(ostream& out, const DoublyLinkedList<T>& dll) {

    DListNode<T> *current = dll.getFirst();
    while (current != dll.getAfterLast()) {
        out << current->obj << " ";
        //iterate through list
        current = current->next;
    }

    return out;
}
