/*
 * @Author: Jason A. Kolodziej 
 * @Copyright (c) 2017-2019 Kolodziej Technologies. All rights reserved. 
 * @Date: 2018-04-08 09:51:43 
 * @Last Modified by: Jason A. Kolodziej
 * @Last Modified time: 2018-04-08 09:52:18
 */
#include "text_freq.h"
#include "my_map.h"
#include "key_value.h"
#include <fstream>
#include <sstream>
using namespace std;

string read_file(string file_name)
{
    //FINISH THIS FUNCTION
    //this should read the file, and return the string of the whole file
    ifstream opener(file_name);
    string r_string;
    string s;
    while(!opener.eof()) {
        r_string+= " "+ s;
        opener >>s;
    }
    return r_string;
}

string remove_punctuation(string& text)
{
    string result;
    std::remove_copy_if(text.begin(), text.end(),            
                        std::back_inserter(result), //Store output           
                        std::ptr_fun<int, int>(&std::ispunct));
    return result;
}

char easytolower(char in){ // helper function
    if(in<='Z' && in>='A')
        return in-('Z'-'z');
    return in;
}

my_map<string, double> create_freq_map(const string& text)
{
    my_map<string,double> freq_map;
    int total_cnt=0;
    //FINISH THIS FUNCTION
    //this should find the frequecies of every word in the text

    string rs;
    for(auto c : text){
        if (' '!=c) rs+=easytolower(c);
        if(!rs.empty() && c==' ') {
            ++total_cnt;
            freq_map[rs] = freq_map[rs] + 1;
            rs = "";
        }
    }
    for(auto &v : freq_map) v.value/=total_cnt;


    return freq_map;
}

vector<key_value<string,double>> vectorize_map(my_map<string, double>& freq_map)
{

    vector<key_value<string,double>> freq_vec;

    //FINISH THIS FUNCTION
    //this should return a sorted vector of the results
    //  for(auto it = freq_map.begin(); it!=freq_map.end(); ++it) freq_vec.push_back(*it);
    auto curr = freq_map.begin();

    while(curr != freq_map.end())
    {
        freq_vec.push_back(*curr);
        curr++;
    }

    return freq_vec;

    return freq_vec;
}

void remove_stop_words(vector<key_value<string,double>>& freq_vec, vector<string> stop_words)
{
   //FINISH THIS FUNCTION
   //this function should remove the elements contained in stop_words from freq_vec
    for(auto s : stop_words)
        for(auto i = freq_vec.begin(); i!=freq_vec.end(); i++)
            if (i->key == s) freq_vec.erase(i);
}

void print_top_20_freqs(const vector<key_value<string,double>>& freq_vec, ostream& out)
{
    vector<key_value<string,double>> resort(freq_vec);
    sort(resort.begin(),resort.end(),[](key_value<string,double> k1, key_value<string,double> k2)
         {
             return k1.value > k2.value;
         });
    int s = resort.size();
    if(s>20) {for(auto i = 0; i<21; ++i) out << resort[i] << endl;}
    else for(auto f : resort) out << f << endl;
}
