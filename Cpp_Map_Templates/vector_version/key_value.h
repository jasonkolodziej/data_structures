/*
 * @Author: Jason A. Kolodziej 
 * @Copyright (c) 2017-2019 Kolodziej Technologies. All rights reserved. 
 * @Date: 2018-04-08 09:51:43 
 * @Last Modified by: Jason A. Kolodziej
 * @Last Modified time: 2018-04-08 09:52:18
 */
#ifndef KEY_VALUE_H
#define KEY_VALUE_H

#include <iostream>

template <typename T, typename E>
struct key_value{
	T key;
	E value;
	key_value(T k = T(), E v = E())
	: key(k), value(v)
	{}  
};

template <typename T, typename E>
bool operator> (const key_value<T, E> &left, const key_value<T, E> &right)
{
    return left.key > right.key;
}

template <typename T, typename E>
bool operator>= (const key_value<T, E> &left, const key_value<T, E> &right)
{
    return left.key >= right.key;
}

template <typename T, typename E>
bool operator< (const key_value<T, E> &left, const key_value<T, E> &right)
{
    return left.key < right.key;
}

template <typename T, typename E>
bool operator<= (const key_value<T, E> &left, const key_value<T, E> &right)
{
    return left.key <= right.key;
}

template <typename T, typename E>
bool operator== (const key_value<T, E> &left, const key_value<T, E> &right)
{
    return left.key == right.key;
}

template <typename T, typename E>
bool operator!= (const key_value<T, E> &left, const key_value<T, E> &right)
{
    return left.key != right.key;
}

template <typename T, typename E>
std::ostream& operator<<(std::ostream& out, const key_value<T, E>& kvp)
{
	
	return out << kvp.key << " " << kvp.value;
}

#endif
