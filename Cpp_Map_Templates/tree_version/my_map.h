/*
 * @Author: Jason A. Kolodziej 
 * @Copyright (c) 2017-2019 Kolodziej Technologies. All rights reserved. 
 * @Date: 2018-04-08 09:51:43 
 * @Last Modified by: Jason A. Kolodziej
 * @Last Modified time: 2018-04-08 09:52:18
 */
#ifndef MY_MAP_H
#define MY_MAP_H

#include "RBTree.h"
#include "key_value.h"
#include <iterator> 
#include <vector>
using namespace std;
template <typename T, typename E>
# define map_node Node<key_value<T,E>>*
struct my_map
{

    class map_iter : public iterator<input_iterator_tag, key_value<T,E>>
    {

    private:
        vector<map_node> prev_nodes; //prev nodes visited

    public:
        map_node node;
        int s = 0;
        map_iter(map_node x) :node(x){}
        map_iter(map_node x, int s) :node(x),s(s){}
        map_iter(const map_iter& iter) : node(iter.node){prev_nodes = iter.prev_nodes;}
        map_iter& operator++() // will invoke the find next function
        {
            //FINISH THIS FUNCTION
            //should go to the next node in the inorder traversal
            if(!is_node_in_prev_nodes(node)) prev_nodes.push_back(node);
            redo:
            if(prev_nodes.size() == s) {
                node = NULL;
                return *this;
            }
            node = this->find_next(node);
            if(!is_node_in_prev_nodes(node)) return *this;
            else goto redo;
//            out:
//                return *this;

        }
        map_iter operator++(int) {map_iter tmp(*this); operator++(); return tmp;}
        bool operator==(const map_iter& rhs) const {return node == rhs.node;}
        bool operator!=(const map_iter& rhs) const {return node != rhs.node;}
        key_value<T,E>& operator*() {return node->value;}

    private:
        void go_to_beginning(); //maybe iterator refresher
        map_node find_next(map_node curr);
        map_node find_smallest_child(map_node curr);
        bool is_node_in_prev_nodes(map_node curr);
    };


    RBTree<key_value<T, E>> container;

    my_map() : container() { }
    my_map(const my_map<T, E>& other);
    my_map<T, E>& operator=( const my_map<T, E>& other );

    E& operator[](T search_key);


public:
    map_iter begin(){
        //FINISH THIS FUNCTION
        //should return the first node in the inorder traversal
        try{
            map_node c = container.root;
            if(!c->left) return map_iter(c,c.size); /**THIS WAS ORIGNALLY NULL**/
            while(c->left!= NULL)
            {
                c = c->left;

            }
            return map_iter(c,container.size);

        }catch(exception& e){cerr << e.what();}

    }

    map_iter end(){
        try{
            return map_iter(NULL);
        }catch(exception& e){cerr << e.what();}
    }

};

template <typename T, typename E>
ostream& operator<<(ostream& out, const my_map<T, E>& map)
{
    try{
        for (auto node : map) out << node;
        return out;
    }catch(exception& e){cerr << e.what();}
}

template <typename T, typename E>
my_map<T,E>::my_map(const my_map<T, E>& other)
    :   container(other.container)
{/**NOTHING NEEDED*/}

template <typename T, typename E>
my_map<T, E>& my_map<T,E>::operator=( const my_map<T, E>& other )
{this->container = other.container;}

template <typename T, typename E>
E& my_map<T,E>::operator[](T search_key)
{
    //FINISH THIS FUNCTION
    //this should search for search_key
    //if it doesent find it, then insert a new key_value into the tree
    try{
        key_value<T,E> k(search_key);
        map_node f = container.search(k); //calls R-B tree search function O(log n)
        if(f) return f->value.value;
        f = container.insert(k);
        return container.bal(f)->value.value; //return a pointer to the value type E in key_value,
        // which is the value stored in the node of the container variable R-B Tree
    }catch(exception& e){cerr << e.what();}

}


template <typename T, typename E>
map_node my_map<T,E>::map_iter::find_next(map_node curr)
{
    //this should find the next node in the inorder traversal
    try{
        map_node a = find_smallest_child(curr);
        if(a) return a;
        else return curr->parent;
        //else if (!is_node_in_prev_nodes(curr->parent->parent)) return curr->parent->parent;
      //  else return NULL;
    }catch(exception& e){cerr << e.what();}
}

template <typename T, typename E>
bool my_map<T,E>::map_iter::is_node_in_prev_nodes(map_node curr)
{
    //this should check if a node has already been visited
    //you can do this with the prev nodes vector, or find a faster way
   // cout << "NODE IS HERE \n";
    return find(prev_nodes.begin(),prev_nodes.end(),curr) != prev_nodes.end(); //if the pointer returned from find is
    //not the end then a node was found in the previous nodes
}

template <typename T, typename E>
void my_map<T,E>::map_iter::go_to_beginning()
{
    //this will go to the beginning of the inorder traversal
   try{
        int n = 0;
       chk:
       if(node > prev_nodes[n]){
           node = prev_nodes[n]; // assign node the lower value
           ++n; //inc. n
           if(n>prev_nodes.size()-1) return; // check out of bounds
           goto chk; //keep going
       }

   } catch(exception& e){cerr << e.what();}
}

template <typename T, typename E>
map_node my_map<T,E>::map_iter::find_smallest_child(map_node curr)
{
    //this function finds the smallest child of curr
    try{
       chk:
            if(curr->left!=NULL && !is_node_in_prev_nodes(curr->left)){
                curr = curr->left;
                goto chk;
            }
            else if(curr->right!=NULL)
            {
                curr = curr->right;
                goto chk;
            }else if(curr->right==NULL && curr->left==NULL && !is_node_in_prev_nodes(curr)) return curr;
            else return NULL;
    }catch(exception& e){cerr << e.what();}
}

#endif
