/*
 * @Author: Jason A. Kolodziej 
 * @Copyright (c) 2017-2019 Kolodziej Technologies. All rights reserved. 
 * @Date: 2018-04-08 09:51:43 
 * @Last Modified by: Jason A. Kolodziej
 * @Last Modified time: 2018-04-08 09:52:18
 */
#include "my_map.h"
#include <iostream>
#include "text_freq.h"
int main()
{
//    my_map<string, int> m;
//    m["c"] = 10;
//    m["f"] = 3;
//    m["b"] = 4;
//m["a"] = 5;
//m["d"] = 15;
//m["e"] = 15;
//m["f"] = 15;
//
//cout << endl << m << endl;
//
//auto curr = m.begin();
//
//while(curr != m.end())
//{
//cout << *curr << endl;
//curr++;
//}


    string file = "test_text1.txt";
    string s = read_file(file);
    string rs = remove_punctuation(s);
    auto m = create_freq_map(rs);
    vector<string> stop = {"the","that","as","and","or"};
    auto v = vectorize_map(m);
    remove_stop_words(v,stop);
    print_top_20_freqs(v,cout);


}