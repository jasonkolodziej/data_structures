/*
 * @Author: Jason A. Kolodziej 
 * @Copyright (c) 2017-2019 Kolodziej Technologies. All rights reserved. 
 * @Date: 2018-01-18 09:51:43 
 * @Last Modified by: Jason A. Kolodziej
 * @Last Modified time: 2019-02-16 09:52:18
 */
#include <iostream>
#include <exception>
#include<queue>

using namespace std;

// Definitions of user defined type exceptions
struct no_item : public exception {
    virtual const char* what() const throw()
    { return "Item NOT Found"; }
};

struct empty_tree : public exception {
    virtual const char* what() const throw()
    { return "Empty Tree"; }
};

struct large_print : public exception {
    virtual const char* what() const throw()
    { return "\nOutput has STOPPED due to LARGE tree"; }
};

struct empty_node : public exception {
    virtual const char* what() const throw()
    { return "Empty Node"; }
};

struct duplicate_item : public exception {
    virtual const char* what() const throw()
    { return "Duplicate Item"; }
};

enum color {null,red, black, db}; // for PA 5

template <typename T>
struct Node
{
	T value; // object node holds
	Node<T>* left; // left child
	Node<T>* right; // right child
    Node<T>* parent = NULL;

    int search_time;
    color node_color; //don't need to worry about this for the regular BTree

    Node(T val, Node<T>* l = NULL, Node<T>* r = NULL, color col = null): value(val), left(l), right(r), search_time(0), node_color(col) {} // constructor
	bool is_leaf(){return (left == 0 && right == 0);} // tells you if the node is a leaf
};

template <typename T>
struct BTree
{
    double cost_summation; // used to solve the average cost of finding a node in the tree
	int size;
	Node<T>* root;

    BTree() : root(NULL),size(0){}
    void cut(Node<T>*); //helper recursive function
    void copy(Node<T>*); //helper recursive function
    ~BTree() {cut(root);} // destructor for BTree
	BTree(const BTree<T>& other); //copy constructor
	BTree<T>& operator=(const BTree<T>& other); //copy assignment

	virtual Node<T>* insert(T obj); // insert object to tree function
	Node<T>* search(T obj); //search the tree for object
	void update_search_times(); // updates the search_time value in each node of tree
    double average_cost(){return cost_summation/size;}; // returns the average search cost in tree
    ostream& inorder(ostream& out, const Node<T>*); //prints the inorder traversal
    virtual ostream& print_level_by_level(ostream& out); // internally called by out stream operator

};

template <typename T>
ostream& operator<<(ostream& out, BTree<T>& tree) //function used by the programmer to unveil tree level by level (provides cleaner coding)
{
    tree.print_level_by_level(out);
    return out;
}

template <typename T>
 ostream& operator<<(ostream& out, const Node<T>* node) { // internally used with in the BTree class as well as help provide cleaner code to the programmer
	if (node->node_color==color::null) return out << node->value << "[" << node->search_time << "] ";
    return out << node->value << "[" << node->search_time << "]{"<< ((node->node_color==color::red) ? "RED": "BLACK") << "} ";
}


template <typename T>
istream& operator>>(istream& in, BTree<T>& tree) { //used during external file usage
    try{
        if(&tree == NULL) throw empty_tree();
        T new_val;
        in >> new_val;
       tree.insert(new_val);
        return in;
    }catch(exception& e) { cerr <<  e.what() << endl; }
}



template <typename T>
BTree<T>::BTree(const BTree<T>& other) { //soft copy a a currently assembled tree
    try{
        root=NULL;
        copy(other.root); //internal recursive helper function
    }catch(exception& e) { cerr <<  e.what() << endl; }

}

template <typename T>
void BTree<T>::cut(Node<T>* r){ // internal, recursive helper function to delete nodes on a tree
    if(r){
        cut(r->right); // recursion
        cut(r->left);  // recursion
        delete r;
    }
}

template <typename T>
void BTree<T>::copy(Node<T>* r) //internal, recurssive helper function to copy a current trees value and add them to a new tree
{
    if(r)
    {
        insert(r->value);
        copy(r->left);  // recursion
        copy(r->right);  // recursion
    }
}



template <typename T>
BTree<T>& BTree<T>::operator=(const BTree<T>& other) {
    try{
        if(this!=&other) {
            cut(root); // erase what is here
            this->root = NULL; // clear out the current root
            copy(other.root); // now start to copy
            this->size = other.size; // change my size to the other tree's size
        }


    }catch(exception& e) { cerr <<  e.what() << endl; }
    return *this;
}


template <typename T>
Node<T>* BTree<T>::insert(T obj) {
try{
    Node<T>* current = search(obj);
    if (!current){// no duplications
        current = new Node<T>(obj);
        if(!root) root = current;// there was no root
        else if (obj < root->parent->value){
            current->parent = root->parent; // value is less than parent
            root->parent->left = current; // value is less than parent
        }
        else{
            current->parent = root->parent; // value is less than parent
            root->parent->right = current; // value was greater than parent
        }
        ++size; // increase the size of the tree
        root->parent = NULL;
        return current;
    }//else throw duplicate_item();

}catch(exception& e) { cerr <<  e.what() << endl; }

}

template <typename T>
Node<T>* BTree<T>::search(T obj) {
try{
    if(root==NULL)  return NULL;
    Node<T>* current = root; // start search @ root
    root->parent = NULL; //use the roots parent node pointer to help us traverse the tree
    while (current && current->value != obj) {
        root->parent = current; // intially the root points at its self
        if (obj < current->value) {
            current = current->left; //points to the left child
        } else {
            current = current->right; //points to the right child
        }
    }
    return current;
}catch(exception& e) { cerr <<  e.what() << endl; }
}


template <typename T>
void BTree<T>::update_search_times() {
    try{
        if (root == NULL)  return;

        // Create an empty queue for level order traversal
        queue<Node<T>*> level;
        int height= 0;
        // Enqueue Root and initialize height
        cost_summation = 0; //set the tree's cost_summation of nodes to zero to reset the current value if any
        level.push(this->root);
        while (true)
        {

            // node_count (queue size) indicates number of nodes
            // at current level.
            int node_count = level.size();
            if (node_count == 0)
                break;

            // dequeue all nodes of current level and enqueue all
            // nodes of next level
            while (node_count > 0)
            {
                Node<T>* node = level.front();
                node->search_time = height+1;
                cost_summation = cost_summation + node->search_time; //adjusts the cost_summantion of the tree when updating individual search times
                level.pop();
                if (node->left != NULL)
                    level.push(node->left);
                if (node->right != NULL)
                    level.push(node->right);
                node_count--;
            }
            height++;

        }
        return;
    }catch(exception& e) { cerr <<  e.what() << endl; }

}


template <typename T>
ostream& BTree<T>::inorder(ostream& out, const Node<T>* tree) {
    try{
        if(tree!=NULL){ // if root is NOT defined as NULL
            inorder(out,tree->left); //recursion
            out << tree;
            inorder(out,tree->right); // recurrsion
        }//else throw empty_node();
    }catch(exception& e) { cerr <<  e.what() << endl; }

	return out;
}


template <typename T>
ostream& BTree<T>::print_level_by_level(ostream& out) {
    try{
        if (root == NULL)  return out;

        // Create an empty queue for level order traversal
        queue<Node<T>*> level;
        int height = 0; //used to help determine the level of 'X' the function is on for NULL nodes
        // Enqueue Root and initialize height
        level.push(root);
        while (true)
        {
            // node_count (queue size) indicates number of nodes
            // at current level.
            int node_count = level.size();
            if (node_count == 0)
                break; //no more nodes
            if(height == 5)
                throw large_print(); //thee output might get messy exit the function now

            // dequeue all nodes of current level and enqueue all
            // nodes of next level

            while (node_count > 0)
            {
                Node<T>* node = level.front();

                out << node;
                level.pop();
                if (node->left != NULL){
                    level.push(node->left);

                }

                if (node->right != NULL){
                    level.push(node->right);
                }
                node_count--;
            }
            out << endl;
            height++; // increase height and aritmetic shift left 1 to the height value to determine  the 'X'
            for(int i = (1<<height)-level.size();i>0;--i) out << "X ";

        }

        out << endl;
        return out;
    }catch(exception& e) { cerr <<  e.what() << endl; }
}
