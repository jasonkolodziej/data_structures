/*
 * @Author: Jason A. Kolodziej 
 * @Copyright (c) 2017-2019 Kolodziej Technologies. All rights reserved. 
 * @Date: 2018-04-03 09:51:43 
 * @Last Modified by: Jason A. Kolodziej
 * @Last Modified time: 2019-04-03 09:52:18
 */
#include "BTree.h"
#ifndef PAS_RBTREE_H
#define PAS_RBTREE_H
template <typename T>
class RBTree: public BTree<T>{
public:
    Node<T>* bal(Node<T>*&a){ return this->balance(a);};
protected:
    void rotate_left(Node<T>*&);
    void rotate_right(Node<T>*&);
    Node<T>* balance(Node<T>*&);
};

template <typename T>
istream& operator>>(istream& in, RBTree<T>& tree) { //used during external file usage
    try{
        if(&tree == NULL) throw empty_tree();
        T new_val;
        in >> new_val;
        Node<T>* unbalanced = tree.insert(new_val);
        tree.bal(unbalanced);
        return in;
    }catch(exception& e) { cerr <<  e.what() << endl; }
}


template <typename T>
void RBTree<T>::rotate_left(Node<T>*&p){
    Node<T> *pt_right = p->right;
    p->right = pt_right->left;
    if (p->right != NULL)p->right->parent = p;
    pt_right->parent = p->parent;
    if (p->parent == NULL) this->root = pt_right;
    else if (p == p->parent->left) p->parent->left = pt_right;
    else p->parent->right = pt_right;
    pt_right->left = p;
    p->parent = pt_right;
}

template <typename T>
void RBTree<T>::rotate_right(Node<T>*&p){
    Node<T> *pt_left = p->left;
    p->left = pt_left->right;
    if (p->left != NULL) p->left->parent = p;
    pt_left->parent = p->parent;
    if (p->parent == NULL) this->root = pt_left;
    else if (p == p->parent->left) p->parent->left = pt_left;
    else p->parent->right = pt_left;
    pt_left->right = p;
    p->parent = pt_left;
}



template <typename T>
Node<T>* RBTree<T>::balance(Node<T>*&p){
    try{
        if(p==NULL) return NULL;
        Node<T> *q = p;
        Node<T> *parent_pt = NULL;
        Node<T> *grand_parent_pt = NULL;
        p->node_color = color::red; //is red before balancing

        while ((p != this->root) && (p->node_color != color::black) &&
               (p->parent->node_color == color::red))
        {

            parent_pt = p->parent;
            grand_parent_pt = p->parent->parent;

            // p's Parent is left child of Grand-parent of p */
            if (parent_pt == grand_parent_pt->left)
            {

                Node<T> *uncle_pt = grand_parent_pt->right;

                //p uncle is also red
                //Only recolor
                if (uncle_pt != NULL && uncle_pt->node_color == color::red)
                {
                    grand_parent_pt->node_color = color::red;
                    parent_pt->node_color = color::black;
                    uncle_pt->node_color = color::black;
                    p = grand_parent_pt;
                }

                else
                {
                    //p is right child of its parent
                    // Left-rotation
                    if (p == parent_pt->right)
                    {
                        rotate_left(parent_pt);
                        p = parent_pt;
                        parent_pt = p->parent;
                    }
                    //p is left child of its parent
                    //Right-rotation
                    rotate_right(grand_parent_pt);
                    swap(parent_pt->node_color, grand_parent_pt->node_color);
                    p = parent_pt;
                }
            }

                // p's parent is right child of p's gp
            else
            {
                Node<T> *uncle_pt = grand_parent_pt->left;
                //p 'uncle is also red
                // Only Recolor
                if ((uncle_pt != NULL) && (uncle_pt->node_color == color::red))
                {
                    grand_parent_pt->node_color = color::red;
                    parent_pt->node_color = color::black;
                    uncle_pt->node_color = color::black;
                    p = grand_parent_pt;
                }
                else
                {
                    //p is left child of its parent
                    // Right-rotation
                    if (p == parent_pt->left)
                    {
                        rotate_right(parent_pt);
                        p = parent_pt;
                        parent_pt = p->parent;
                    }
                    //p is right child of its parent
                    // Left-rotation
                    rotate_left(grand_parent_pt);
                    swap(parent_pt->node_color, grand_parent_pt->node_color);
                    p = parent_pt;
                }
            }
        }
        // make sure root is always black
        this->root->node_color = color::black;
        return q;
    }catch(exception& e) { cerr <<  e.what() << endl; }

}


#endif //PAS_RBTREE_H
