/*
 * @Author: Jason A. Kolodziej 
 * @Copyright (c) 2017-2019 Kolodziej Technologies. All rights reserved. 
 * @Date: 2018-04-08 09:51:43 
 * @Last Modified by: Jason A. Kolodziej
 * @Last Modified time: 2018-04-08 09:52:18
 */
#include "flight.h"

Flight::Flight()
{
}

Flight::Flight(std::string fn, std::string dest, std::string dt, std::string gn)
            :flightNum{fn},destination{dest},departureTime{dt},gateNum{gn}
{
  if( dt.size()==4) departureTime = "0"+dt; //checks to see if a time 7:40 instead of 07:40...7:40 is an arbitrary time
}

Flight::~Flight()
{

}

bool compareToDestination(Flight f1, Flight f2) // returns TRUE if f1 is alphabetically ranked higher or equal to that f2
{
    return f1.destination >= f2.destination; //uses lexicographical comparison
}

bool compareToDepartureTime(Flight f1, Flight f2) // returns TRUE if f1 has a equal to or later time than f2
{
    return f1.departureTime >= f2.departureTime;
}
