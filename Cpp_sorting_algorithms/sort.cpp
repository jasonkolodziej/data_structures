/*
 * @Author: Jason A. Kolodziej 
 * @Copyright (c) 2017-2019 Kolodziej Technologies. All rights reserved. 
 * @Date: 2018-04-08 09:51:43 
 * @Last Modified by: Jason A. Kolodziej
 * @Last Modified time: 2018-04-08 09:52:18
 */

#include "sort.h"
#include <stdio.h>
#include <iostream>


//use this to keep track of comparisons
int num_cmps=0;

std::vector<Flight> selection_sort(std::vector<Flight> flights,
				   SortOption sortOpt)
{
    int i, j;
    Flight hldr;
    switch(sortOpt)
    {
        case SortOption::ByDestination:
            for(i=0;i<flights.size();i++)
            {
                for(j=i+1;j<flights.size();j++)
                {
                    if(compareToDestination(flights[i],flights[j]))//if(flights[j]<flights[i]) //for descending order use if(a[j]>a[i])
                    {
                        hldr=flights[i];
                        flights[i]=flights[j];
                        flights[j]=hldr;
                    }
                    num_cmps++;
                }
            }
                //std::swap(&flights[min_idx], &flights[i]);
            break;
        default:
            // One by one move boundary of unsorted vector
            for(i=0;i<flights.size();i++)
            {
                for(j=i+1;j<flights.size();j++)
                {
                    if(compareToDepartureTime(flights[i],flights[j]))//if(flights[j]<flights[i]) //for descending order use if(a[j]>a[i])
                    {
                        hldr=flights[i];
                        flights[i]=flights[j];
                        flights[j]=hldr;
                    }
                    num_cmps++;
                }
            }
            break; //sorts by departure time
    }
    std::cout<<num_cmps;
  return flights;
}

std::vector<Flight> insertion_sort(std::vector<Flight> flights,
				   SortOption sortOpt) {
    int k, j;
    Flight tmp;
    switch (sortOpt) {
        case SortOption::ByDestination:
            for (k = 1; k < flights.size(); k++) {
                j = k;
                while (j > 0 && compareToDestination(flights[j - 1], flights[j])) { //while (j > 0 && flights[j - 1] > flights[j]) {
                    tmp = flights[j];
                    flights[j] = flights[j - 1];
                    flights[j - 1] = tmp;
                    num_cmps++;
                    j--;

                }
            }
            break;
        default:
            for (k = 1; k < flights.size(); k++) {
                j = k;
                while (j > 0 && compareToDepartureTime(flights[j-1],flights[j])) { //while (j > 0 && flights[j - 1] > flights[j]) {
                    tmp = flights[j];
                    flights[j] = flights[j - 1];
                    flights[j - 1] = tmp;
                    num_cmps++;
                    j--;

                }
            }
            break; //  Departure time by default
    }
    std::cout<<num_cmps;
    return flights;
}

std::vector<Flight> bubble_sort(std::vector<Flight> flights,
				SortOption sortOpt)
{
    int k ,j;
    bool cont = false;
    Flight tmp;
    switch(sortOpt)
    {
        case SortOption::ByDestination:
            for (k = 1; k < flights.size(); k++) {
                for (int j = 0; j < flights.size() - k; j++)
                    if (compareToDestination(flights[j],flights[j+1])) {//if (flights[j + 1].destination < flights[j].destination) {
                        tmp = flights[j]; // swap flights[j] and flights[j+1]
                        flights[j] = flights[j + 1];
                        flights[j + 1] = tmp;
                        cont = true;
                    }
                num_cmps++;
                if (!cont) break; // stop sorting vector
            }
            break;

        default: //sorts by departure time
            for (int k = 1; k < flights.size(); k++) {
                for (int j = 0; j < flights.size() - k; j++)
                    if (compareToDepartureTime(flights[j],flights[j+1])) {//if (flights[j + 1].departureTime < flights[j].departureTime) {
                        tmp = flights[j]; // swap flights[j] and flights[j+1]
                        flights[j] = flights[j + 1];
                        flights[j + 1] = tmp;
                        cont = true;
                    }
                num_cmps++;
                if (!cont) break; // stop sorting vector
            }
            break;
    }
    std::cout<<num_cmps;
  return flights;
}
