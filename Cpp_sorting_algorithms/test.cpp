/*
 * @Author: Jason A. Kolodziej 
 * @Copyright (c) 2017-2019 Kolodziej Technologies. All rights reserved. 
 * @Date: 2018-04-08 09:51:43 
 * @Last Modified by: Jason A. Kolodziej
 * @Last Modified time: 2018-04-08 09:52:18
 */
///THIS IS THE MAIN CPP FILE


#include <sstream>
#include <iostream>
#include <fstream>
#include <iomanip> // used for table printing to file
#include "sort.h"

std::vector<Flight> readFlights(std::string fileName);
void make_table(std::vector<Flight>, std::ofstream&);

int main()
{

    //first read flights in files using readFlights()
     std::string csvFiles = "rand10.csv";
    std::vector<Flight> table;
    std::ofstream saveFile("output.txt");
    table = readFlights(csvFiles);

    saveFile << "SELECTION SORT | BY DEPARTURE TIME\n";
    make_table(selection_sort(table,SortOption::ByDepartureTime), saveFile);
    saveFile << "SELECTION SORT | BY DESTINATION\n";
    make_table(selection_sort(table,SortOption::ByDestination), saveFile);

    saveFile << "INSERTION SORT | BY DEPARTURE TIME\n";
    make_table(insertion_sort(table,SortOption::ByDepartureTime), saveFile);
    saveFile << "INSERTION SORT | BY DESTINATION\n";
    make_table(insertion_sort(table,SortOption::ByDestination), saveFile);

    saveFile << "BUBBLE SORT | BY DEPARTURE TIME\n";
    make_table(bubble_sort(table,SortOption::ByDepartureTime), saveFile);
    saveFile << "BUBBLE SORT | BY DESTINATION\n";
    make_table(bubble_sort(table,SortOption::ByDestination), saveFile);


    /*
    then use each of the sorting functions on each of the generated vectors
  */

  /*
    then output each of the resultant sorted vectors
    format them so that they look like the table of the front of the 
    instructions

    Flight Number	Destination Departure   Time	        Gate Number
    AA223			LAS VEGAS	21:15			A3
    BA023			DALLAS		21:00			A3
    AA220			LONDON		20:30			B4
    VI303			MEXICO		19:00			A7
    BA087			LONDON		17:45			A7
    AA342			PARIS		16:00			A7
    VI309			PRAGUE		13:20			F2
    QU607			TORONTO		08:30			F2
    AA224			SYDNEY		08:20			A7
    AF342			WASHINGTON	07:45			A3
  */


  /*
    then fill out the rest of the questions on the instructions 
    - the number of comparisons

    - to test your functions experimentally use difftime explained here 
    http://www.cplusplus.com/reference/ctime/time/
  */
  return 0;
}

//read in the flights from the input file at fileName and store them in a vector

void make_table(std::vector<Flight> table, std::ofstream& saveFile)
{
    const char separator    = ' ';
    saveFile << std::left << std::setw(30) << std::setfill(separator) << "Flight Number";
    saveFile << std::left << std::setw(30) << std::setfill(separator) << "Destination";
    saveFile << std::left << std::setw(30) << std::setfill(separator) << "Departure Time";
    saveFile << std::left << std::setw(30) << std::setfill(separator) << "Gate Number" << std::endl;
    for(auto flight : table)
    {
        saveFile << std::left << std::setw(30) << std::setfill(separator) << flight.flightNum;
        saveFile << std::left << std::setw(30) << std::setfill(separator) << flight.destination;
        saveFile << std::left << std::setw(30) << std::setfill(separator) << flight.departureTime;
        saveFile << std::left << std::setw(30) << std::setfill(separator) << flight.gateNum << std::endl;
    }
    saveFile << std::endl;
}

std::vector<Flight> readFlights(std::string fileName)
{
  std::vector<Flight> flights;
    std::ifstream openFile (fileName);
    std::string flight;
    std::string depart;
    std::string dest;
    std::string gate;
    while(!openFile.eof()){
       getline(openFile,flight,','); getline(openFile,dest,','); getline(openFile,depart,','); getline(openFile,gate);
        if(dest == "DESTINATION" || dest == "destination");
        else {
            gate.erase(gate.end()-1, gate.end()); // removes the return cartridge character and sets to NULL
            Flight f(flight,dest,depart,gate);
            flights.push_back(f);
        }
    }
    openFile.close();
    flights.erase(flights.end()-1,flights.end());
  return flights;
}

